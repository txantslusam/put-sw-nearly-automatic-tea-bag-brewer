/*Accept command from BBB via Serial
 *Turns on/off on-board LED
 *by OP from teachmemicro.com */

#include <SoftwareSerial.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,20,4);
SoftwareSerial mySerial(8, 7); // RX, TX

char inChar;
void setup()
{
    // Open serial communications and wait for port to open:
    Serial.begin(9600);
    Serial.println("Awaiting command from the Almighty BBB...");
    // set the data rate for the SoftwareSerial port
    mySerial.begin(9600);
    pinMode(13, OUTPUT);

    lcd.begin(16,2);
    lcd.init();
    lcd.init();
    lcd.backlight();
    lcd.setCursor(0,0);
}

void loop() // run over and over
{
  if (mySerial.available()){
    inChar = (char)mySerial.read();
    if(inChar=='1'){
      Serial.print("The Almighty BBB said Let There Be Light! ");
      Serial.println("Then there was light!");
      digitalWrite(13, HIGH);
    }else if(inChar =='0'){
      Serial.print("The Almighty BBB said Let There Be Darkness! ");
      Serial.println("Then darkness commenced!");
      digitalWrite(13, LOW);
    }else{
      Serial.print("The Almighty BBB said Something! ");
      Serial.println("We fear that something!");
   }
  }
}