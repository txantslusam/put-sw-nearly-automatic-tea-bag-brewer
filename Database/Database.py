import sqlite3
import json


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class DatabaseConnection:
    def __init__(self):
        self.connection = sqlite3.connect('teas.db')
        self.connection.row_factory = dict_factory
        self.create_table()

        if self.get_records_count() == 0:
            self.seedDB()



    def create_table(self):
        c = self.connection.cursor()
        c.execute('''
            CREATE TABLE IF NOT EXISTS teas (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT,
                temperature INTEGER,
                time INTEGER
            )
        ''')
        self.connection.commit()

    def get_records_count(self):
        c = self.connection.cursor()
        c.execute('SELECT * FROM teas')
        return len(c.fetchall())

    def seedDB(self):
        with open('Database/ExampleData.json') as f:
            exampleData = json.load(f)
            for record in exampleData:
                self.create(record)

    def find_all(self):
        c = self.connection.cursor()
        c.execute('SELECT * FROM teas')
        return c.fetchall()

    def create(self, tea):
        c = self.connection.cursor()
        c.execute('INSERT INTO teas (name, temperature, time) VALUES (?, ?, ?)',
                  (tea["name"], tea["temperature"], tea["time"]))
        self.connection.commit()
        return self.find_all()

    def update(self, tea):
        c = self.connection.cursor()
        c.execute('UPDATE teas SET name=?, temperature=?, time=? WHERE id=?',
                  (tea["name"], tea["temperature"], tea["time"], tea["id"]))
        self.connection.commit()
        return self.find_all()

    def delete(self, id):
        c = self.connection.cursor()
        c.execute('DELETE FROM teas WHERE id=?', (id, ))
        self.connection.commit()
        return self.find_all()