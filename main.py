from Database.Database import DatabaseConnection
from Components import Button, LCD, Servo, TemperatureSensor
import time

SERVO_PIN = "P9_14"

CONFIRM_BUTTON_PIN = "P9_12"
PREV_BUTTON_PIN = "P9_11"
NEXT_BUTTON_PIN = "P9_13"

THERMOMETER_PIN = "P9_40"

LCD_PINS = {
    "rs": "P8_8",
    "en": "P8_10",
    "d4": "P8_18",
    "d5": "P8_16",
    "d6": "P8_14",
    "d7": "P8_12",
    "backlight": "P8_7",

    "rx": "P9_26",
    "tx": "P9_24"
}


class Main:
    def __init__(self):
        self.db = DatabaseConnection()
        self.servo = Servo.Servo(SERVO_PIN)
        self.confirmButton = Button.Button(CONFIRM_BUTTON_PIN)
        self.prevButton = Button.Button(PREV_BUTTON_PIN)
        self.nextButton = Button.Button(NEXT_BUTTON_PIN)
        self.temperatureSensor = TemperatureSensor.TemperatureSensor(THERMOMETER_PIN)
        self.lcd = LCD.LCD(LCD_PINS)

        self.inBrewingMode = False
        self.teas = self.db.find_all()
        self.selectedTea = {}
        self.dbCursor = 0

        self.loop()

    def loop(self):
        while True:
            if self.inBrewingMode:
                self.brewing_mode_handler()
            else:
                self.selection_mode_handler()

    def selection_mode_handler(self):
        if self.prevButton.get_value() == 1:
            print("prevButtton\n")
            if self.dbCursor == 0:
                self.dbCursor = len(self.teas) - 1
            else:
                self.dbCursor -= 1
            self.display_tea_on_LCD()

        elif self.nextButton.get_value() == 1:
            print("nextButtton\n")
            if self.dbCursor == len(self.teas) - 1:
                self.dbCursor = 0
            else:
                self.dbCursor += 1
            self.display_tea_on_LCD()

        elif self.confirmButton.get_value() == 1:
            print("confirmButtton\n")
            self.selectedTea = self.teas[self.dbCursor]
            self.inBrewingMode = True

    def brewing_mode_handler(self):
        temperature = self.temperatureSensor.getTemperature()
        target_temperature = self.selectedTea["temperature"]

        if target_temperature - 1 < temperature < target_temperature + 1:
            self.servo.rotate(1)

            for i in range(self.selectedTea["time"]):
                self.lcd.sendMessage(str(i) + " secs")
                time.sleep(1)

            self.servo.rotate(1)
            self.lcd.sendMessage('Brewing end')
            self.inBrewingMode = False
        else:
            self.lcd.sendMessage(f'{temperature} out of target {target_temperature}')
            return

    def display_tea_on_LCD(self):
        actual_tea = self.teas[self.dbCursor]
        message = actual_tea["name"] + "\nTime: " + str(actual_tea["time"]) + " Temp: " + str(actual_tea["temperature"])
        self.lcd.sendMessage(message)


if __name__ == '__main__':
    Main()
