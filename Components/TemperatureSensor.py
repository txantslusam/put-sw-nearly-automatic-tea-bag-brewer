import time
from numpy import mean
# import Adafruit_BBIO.ADC as ADC

class TemperatureSensor:
    def __init__(self, pin):
        self.pin = pin
        # ADC.setup()

    def getTemperature(self):
        counter = 10
        temperatures = []
        for _ in range(counter):
            # reading = ADC.read(self.pin)
            reading = 1
            millivolts = reading * 1800
            temp_c = (millivolts - 500) / 10
            temperatures.append(temp_c)
            time.sleep(0.2)

        temperatures.sort()
        return mean(temperatures[1:len(temperatures) - 2])
