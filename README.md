# Nearly automatic tea bag brewer

A python program for BeagleBone Black platform

## Dependencies

- `sqlite3`
- `numpy`
- `Adafruit_BBIO`
