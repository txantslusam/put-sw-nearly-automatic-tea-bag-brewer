from Database.Database import DatabaseConnection


class Cli:
    def __init__(self):
        self.db = DatabaseConnection()
        self.teas = []

        self.print_instruction()
        self.loop()

    def print_instruction(self):
        print('''
            Tea brewer CLI:
            c - create new tea
            u - update tea
            d - delete tea
            g - get all teas
            i - instruction
            q - quit
        ''')

    def loop(self):
        while True:
            option = input("Select option: ")
            if option == 'c':
                self.create_tea()
            elif option == 'u':
                self.update_tea()
            elif option == 'd':
                self.delete_tea()
            elif option == 'g':
                self.get_all_teas()
            elif option == 'i':
                self.print_instruction()
            elif option == 'q':
                break

    def create_tea(self):
        name = input("Type name: ")
        temperature = int(input("Type temperature: "))
        time = int(input("Type time: "))
        self.db.create({
            "name": name,
            "temperature": temperature,
            "time": time
        })
        print("Tea added")

    def update_tea(self):
        self.teas = self.db.find_all()
        tea_id = int(input("Type id: "))
        name = input("Type name (leave blank to skip): ")
        temperature = input("Select temperature (leave blank to skip): ")
        time = input("Select time (leave blank to skip): ")

        first_or_default = next((tea for tea in self.teas if tea["id"] == tea_id), None)
        if first_or_default is None:
            print("Tea with typed id does not exist")
            return

        self.db.update({
            "id": tea_id,
            "name": name if len(name) > 0 else first_or_default["name"],
            "temperature": int(temperature) if len(temperature) > 0 else first_or_default["temperature"],
            "time": int(time) if len(time) > 0 else first_or_default["time"],
        })

        print("Update successful")

    def delete_tea(self):
        self.teas = self.db.find_all()
        tea_id = int(input("Type id: "))

        first_or_default = next((tea for tea in self.teas if tea["id"] == tea_id), None)
        if first_or_default is None:
            print("Tea with typed id does not exist")
            return

        self.db.delete(tea_id)
        print("Delete successful")

    def get_all_teas(self):
        self.teas = self.db.find_all()
        for tea in self.teas:
            print(f'Tea id: {tea["id"]}\n'
                  f'Name: {tea["name"]}\n'
                  f'Temperature: {tea["temperature"]}\n'
                  f'Time: {tea["time"]}\n'
                  f'==========================')


if __name__ == "__main__":
    Cli()
